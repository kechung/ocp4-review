# (c) 2016, Matt Martz <matt@sivel.net>
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

# Make coding more python3-ish
# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
import os
import sys
import shutil
import glob

# sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../python-routines/asciidoctorgenerator.py")
# from asciidoctorgenerator import *

from ansible.plugins.callback import CallbackBase


class CallbackModule(CallbackBase):
    # CALLBACK_VERSION = 2.0
    # CALLBACK_TYPE = 'stdout'
    # CALLBACK_NAME = 'json'

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'notification'
    CALLBACK_NAME = 'json-custom'
    CALLBACK_NEEDS_WHITELIST = True
    # PATH_REPORT=os.path.dirname(os.path.abspath(__file__));

    def __init__(self, display=None):
        super(CallbackModule, self).__init__(display)
        self.results = []

    def _new_play(self, play):
        return {
            'play': {
                'name': play.name,
                'id': str(play._uuid)
            },
            'tasks': []
        }

    def _new_task(self, task):
        return {
            'task': {
                'name': task.name,
                'id': str(task._uuid)
            },
            'hosts': {}
        }

    def v2_playbook_on_play_start(self, play):
        self.results.append(self._new_play(play))

    def v2_playbook_on_task_start(self, task, is_conditional):
        self.results[-1]['tasks'].append(self._new_task(task))

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        self.results[-1]['tasks'][-1]['hosts'][host.name] = result._result

    def v2_playbook_on_stats(self, stats):
        """Display info about playbook statistics"""

        hosts = sorted(stats.processed.keys())

        summary = {}
        for h in hosts:
            s = stats.summarize(h)
            summary[h] = s

        output = {
            'plays': self.results,
            'stats': summary
        }

        print(json.dumps(output, indent=4, sort_keys=True))
        facts_location=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/";
        facts_file=facts_location+"play-data.json"
        if not os.path.exists(facts_location):
            os.makedirs(facts_location)
        with open(facts_file, "w") as text_file:
            text_file.write(json.dumps(output, indent=4, sort_keys=True))
        self.asciidoctorgenerator(output)

    def asciidoctorgenerator(self, facts):
        print("Starting generate-asciidoc()")
        # facts_file=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/test.json"
        result_folder=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/";

        checklist_data = dict()
        checklist_file_list = []
        checklist_data_list = []

        print("Deleting old files in chapters/appendix")
        for f in glob.glob(result_folder+"chapters/appendix/data/*"):
            os.remove(f)
        for f in glob.glob(result_folder+"chapters/appendix/checklists/*"):
            os.remove(f)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print(type(facts))
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print(facts)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        for item in facts['plays']:
            # Creating a new file
            fname=result_folder+"chapters/appendix/data/"+item['play']['name']+"-"+item['play']["id"]+".adoc"
            checklist_data_list.append("data/"+item['play']['name']+"-"+item['play']["id"]+".adoc");

            with open(fname, "w") as text_file:
                self.printLine(text_file, "=== "  +item['play']['name'] +" - Supporting data")
                self.printLine(text_file, ". Play: "    +item['play']['name'])
                self.printLine(text_file, ". Play ID: " +item['play']['id'])
                self.printLine(text_file, ". Size: "+   str(len(item['tasks'])))

                self.printLine(text_file,"\n ");
                tasksName = list()
                for task in item['tasks']:
                    if task['task']['name'] and "nodoc" not in str(task['task']['name']):
                        tasksName.append(task['task']['name'])
                        self.printLine(text_file,"==== "+ task['task']['name'])
                        #print("=============")
                    	#print(task['hosts'])
                        #print("=============")
                        #print("=============")
                        for host,invhost in task['hosts'].items():
                            print("=============================================================================")
                            print(invhost)
                            print("=============================================================================")
                            if "invocation" in invhost:
                                raw_cmd_tmp =invhost['invocation']['module_args']
                                if '_raw_params' in raw_cmd_tmp:
                                    raw_cmd =invhost['invocation']['module_args']['_raw_params']
                                    print("======RAW CMD=======")
                                    print(raw_cmd)
                                    print("======FIN RAW CMD=======")
                                    #self.printLine(text_file,"."+host+": "+invhost['invocation']['module_name'])
                                    #self.printLine(text_file,"."+host+": "+invhost['invocation']['module_args']['_raw_params'])
                                    self.printLine(text_file,".Host: "+host)
                                    if "cat {}" not in raw_cmd:
                                        self.printLine(text_file,"[source,bash]")
                                        self.printLine(text_file,"----")
                                        self.printLine(text_file,"$ "+ str(raw_cmd))
                                        if invhost['stdout_lines']:
                                            for line in invhost['stdout_lines']:
                                                self.printLine(text_file, str(line.encode('utf-8')))
                                        else:
                                            self.printLine(text_file, invhost['stderr'])
                                        self.printLine(text_file,"----")
                                        self.printLine(text_file,"\n ");
                                    else:
                                        self.printLine(text_file,"[source,bash]")
                                        self.printLine(text_file,"----")
                                        self.printLine(text_file,"$ "+ str(raw_cmd))
                                        self.printLine(text_file,"----")
                                        self.printLine(text_file,"\n ");
                                        #seccion de archivos
                                        self.printLine(text_file,"====")
                                        if invhost['stdout_lines']:
                                            for line in invhost['stdout_lines']:
                                                self.printLine(text_file, str(line))
                                        else:
                                            self.printLine(text_file, invhost['stderr'])
                                        self.printLine(text_file,"\n ");
                            else:
                                if "results" in invhost:
                                    print(len(invhost['results']))
                                    results_tmp =invhost['results']
                                    self.printLine(text_file,"[source,bash]")
                                    for result in results_tmp:
                                        print(result['cmd'])
                                        self.printLine(text_file, result['cmd']);
                                        print("*********** LARGO STDOUT*****************")
                                        print(len(result['stdout_lines']))
                                        print("*****************************************")
                                        if len(result['stdout_lines']) > 1:
                                            multi_stdout=result['stdout_lines']
                                            for rs in multi_stdout:
                                                if "#" not in rs and "sh" not in rs:
                                                    self.printLine(text_file, rs);
                                        else:
                                            print(result['stdout_lines'][0])
                                            self.printLine(text_file, result['stdout_lines'][0]);
                                    self.printLine(text_file,"\n ");
                #fin seccion de archivos
                print("-----------------------------------")
                print("INICIANDO CHECKLIST")
                print("-----------------------------------")
                checklist_data[item['play']['name']] = tasksName;
                checklist_folder=result_folder+"chapters/appendix/checklists/"
                print(checklist_data)
                print("----------------------------------------------")
                for key, value in checklist_data.items():
                    print("=============")
                    print(key)
                    print("=============")
                    print(value)
                    chkfilename=checklist_folder+"anexo_"+key+".adoc"
                    checklist_file_list.append("checklists/anexo_"+key+".adoc")

                    print(checklist_file_list)
                    with open(chkfilename, "w") as chkfile:
                        self.printLine(chkfile,"=== Checklist "+ key )
                        self.printLine(chkfile,"\n")
                        self.printLine(chkfile,"====")
                        self.printLine(chkfile,".Checklist "+key)
                        self.printLine(chkfile,"//[width=\"100%\", cols=\"^1,^1,6,16\", frame=\"topbot\",options=\"header\"]")
                        self.printLine(chkfile,"[width=\"100%\", cols=\"^1,^1,6,16\", frame=\"topbot\",options=\"header\"]")
                        self.printLine(chkfile,"|======================")
                        self.printLine(chkfile,"|#       \n|  \n| Item    \n| Comments")
                        self.printLine(chkfile,"\n")
                        cont = 0
                        for taskItem in value:
                            if "nodoc" not in str(taskItem):
                                cont = cont + 1
                            	# self.printLine(chkfile, ""+str(taskItem))
                            	# print str.replace("is", "was")
                                self.printLine(chkfile,"| "+str(cont))
                                self.printLine(chkfile,"| image:warn.png[]")
                                self.printLine(chkfile,"| "+str(taskItem).replace("|", "\|"))
                                self.printLine(chkfile,"| It must be completed after the analysis work on data collected")
                                self.printLine(chkfile,"\n")
                        self.printLine(chkfile,"|======================")
                        self.printLine(chkfile,"====")


        ## Data Files creating process
        fname=result_folder+"chapters/appendix/appendix_checklist.adoc"
        with open(fname, "w") as chkfile:
            self.printLine(chkfile,"== Red Hat Openshift Cluster Review ")
            for item in set(checklist_file_list):
                if "INIT" not in item:
                    self.printLine(chkfile,"\n")
                    self.printLine(chkfile,"include::"+item+"[]")
                # checklist_file_list.append("checklists/anexo_"+key+".adoc")
        fname=result_folder+"chapters/appendix/appendix_data.adoc"
        with open(fname, "w") as chkfile:
            self.printLine(chkfile,"==  ")
            for item in set(checklist_data_list):
                if "INIT" not in item:
                    self.printLine(chkfile,"\n")
                    self.printLine(chkfile,"include::"+item+"[]")

    def str_unicode(self,text):
        try:
            text = unicode(text, 'utf-8')
        except TypeError:
            return text

    def printLine(self,file, texto):
        print("PRINT LINE: "+ texto)
        file.write(texto+"\n");


    v2_runner_on_failed = v2_runner_on_ok
    v2_runner_on_unreachable = v2_runner_on_ok
    v2_runner_on_skipped = v2_runner_on_ok
